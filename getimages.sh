#!/bin/bash
# Get all product images using extracted list of productids
for productid in $(cat ./data/productids_of_interest.txt); do
	wget http://media.threadless.com/product/190x180/$productid-minizoom.jpg -O ./data/images/productid_$productid.jpg
	sleep 1
done