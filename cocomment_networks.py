import networkx
import csv

with open('./data/prior_cocomment/network_stats.csv', 'wb') as f_stats:
	stats_header = ['submissionid', '# userids', '# edges']
	stats_writer = csv.DictWriter(f_stats, stats_header)
	stats_writer.writeheader()
	for design_type in ('popular', 'unpopular'):
		with open('./data/%s_submissionids.txt' % design_type, 'rb') as f:
			all_submissionids = [l.strip() for l in f.readlines()]
		for submissionid in all_submissionids:
			print 'Working on the %s submissionid %s.' % (design_type, submissionid)
			G = networkx.read_edgelist('./data/edgelists/%s/submissionid_%s.edgelist' % (design_type, submissionid))
			blogids, userids = networkx.bipartite.sets(G)
			H = networkx.bipartite.weighted_projected_graph(G, userids)
			num_userids = len(userids)
			num_edges = len(H.edges())
			print '%d userids and %d edges in co-comment network.' % (num_userids, num_edges)
			stats_writer.writerow(dict(zip(stats_header, [submissionid, num_userids, num_edges])))
			with open('./data/prior_cocomment/%s/submissionid_%s.csv' % (design_type, submissionid), 'wb') as f:
				headers = ['userid1', 'userid2', '# product or design blogs co-commented on previously']
				dw = csv.DictWriter(f, headers, delimiter=",")
				dw.writeheader()
				for edge in H.edges(data=True):
					# Strip out the text prefix we used to distinguish userid nodes from
					# blogid nodes in the networkx graph object
					userid1 = edge[0][len('userid'):]
					userid2 = edge[1][len('userid'):]
					weight = edge[2]['weight']
					row_to_write = dict(zip(headers, (userid1, userid2, weight)))
					dw.writerow(row_to_write)
