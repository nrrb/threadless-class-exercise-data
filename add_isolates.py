#!python
import glob
import csv
import os

parcel_roots = glob.glob('./data/parcels/*')
for parcel_root in parcel_roots:
	print 'In parcel %s.' % parcel_root
	with open(os.path.join(parcel_root, 'user_attributes.csv'), 'rb') as f:
	    dr = csv.DictReader(f)
	    users = list(dr)
	with open(os.path.join(parcel_root, 'prior_cocommenting_network.csv'), 'rb') as f:
	    dr = csv.DictReader(f)
	    network_edges = list(dr)
	users_in_network = set([edge['userid1'] for edge in network_edges] + [edge['userid2'] for edge in network_edges])
	all_users = set([user['userid'] for user in users])
	isolate_users = all_users.difference(users_in_network)
	print 'Writing %d isolates.' % len(isolate_users)
	with open(os.path.join(parcel_root, 'prior_cocommenting_network-w_isolates.csv'), 'wb') as f_out:
		with open(os.path.join(parcel_root, 'prior_cocommenting_network.csv'), 'rb') as f_in:
			f_out.write(f_in.read())
		for user in isolate_users:
			new_line = '%s,%s,0\r\n' % (user, user)
			f_out.write(new_line)