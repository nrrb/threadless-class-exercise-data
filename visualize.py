from visualize_data import *
# import os

# def find_files_by_extension(search_path, extension):
# 	all_files = []
# 	for root, dirs, files in os.walk(search_path):
# 		for file in files:
# 			if file.split('.')[-1].lower() == extension:
# 				all_files.append(os.path.join(root, file))
# 	return all_files

def image_filename(productid):
	return '/home/nick/Dropbox/code/Threadless-Brooke/data/images/productid_%d.jpg' % productid

def image_html(image_path, rank, sales):
	alt_text = '#%d design with %d sales per year' % (rank, sales)
	return '<a href="file://%s"><img src="%s" width="67" height="63" alt="%s" title="%s"/></a>' % (image_path, image_path, alt_text, alt_text)
#	return '<img src="%s" width="67" height="63" alt="%s" title="%s"/>' % (image_path, alt_text, alt_text)

image_path_by_rank = {}
sales_by_rank = {}
for rank, productid, sales_per_year in product_id_ranking:
	image_path_by_rank[rank] = image_filename(productid)
	sales_by_rank[rank] = int(sales_per_year)

with open('visualize.html', 'wb') as f:
	f.write('<html><body>')
	for row in picture_ordering:
		html = ''.join(map(lambda x: image_html(image_path_by_rank[x], x, sales_by_rank[x]), row)) + '<br />\r\n'
		f.write(html)
	f.write('</body></html>')
