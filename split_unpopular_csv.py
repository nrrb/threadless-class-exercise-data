import csv

for design_type in ('popular', 'unpopular'):
	print 'Working on %s submissionids.' % design_type
	files = dict()
	with open('./data/prior_commenting_%s_sorted.csv' % design_type, 'rb') as f:
		reader = csv.reader(f, delimiter=",", quotechar='"')
		for submissionid, blogid, userid in reader:
			if submissionid not in files:
				print 'Creating file object for submissionid %s.' % submissionid
				files[submissionid] = open('./data/edgelists/%s/submissionid_%s.edgelist' % (design_type, submissionid), 'wb')
			files[submissionid].write('blogid%s userid%s\r\n' % (blogid, userid))
	for submissionid, f in files.iteritems():
		f.close()
	with open('./data/%s_submissionids.txt'%design_type, 'wb') as f:
		f.writelines([submissionid + '\r\n' for submissionid in files.keys()])
