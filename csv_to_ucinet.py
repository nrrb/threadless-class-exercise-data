#!python
import glob
import csv
import os

header = '''DL n=%d
format = edgelist1
labels embedded
data:
'''

parcel_roots = glob.glob('./data/parcels/*')
for parcel_root in parcel_roots:
	print 'In parcel %s.' % parcel_root
	with open(os.path.join(parcel_root, 'prior_cocommenting_network.csv'), 'rb') as f:
	    dr = csv.DictReader(f)
	    network_edges = list(dr)
	users_in_network = set([edge['userid1'] for edge in network_edges] + [edge['userid2'] for edge in network_edges])
	with open(os.path.join(parcel_root, 'UCINET-prior_cocommenting.txt'), 'wb') as f:
		f.write(header % len(users_in_network))
		for edge in network_edges:
			userid1 = edge['userid1']
			userid2 = edge['userid2']
			weight = edge['# product or design blogs co-commented on previously']
			f.write('%s %s %s\r\n' % (userid1, userid2, weight))

