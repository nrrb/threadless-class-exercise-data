import shutil
import xlrd
import csv
import os

def xls_row_values(row):
	row_values = map(lambda x: x.value, row)
	for i, value in enumerate(row_values):
		if isinstance(value, float):
			row_values[i] = int(value)
	return row_values

save_path = './data/parcels/'

design_popularity = {}
# Extract the submissionid stats into a single CSV file
wb = xlrd.open_workbook('./data/submissionid_info.xls')
datasheet = wb.sheets()[0]
submissionid_info = {}
sub_info_header = xls_row_values(datasheet.row(0))
for row_index in xrange(1, datasheet.nrows):
	row = datasheet.row(row_index)
	submissionid = int(row[0].value)
	popularity = row[-1].value
	design_popularity[submissionid] = popularity
	submissionid_info[submissionid] = xls_row_values(row)
for submissionid in submissionid_info:
# Make all the directories ahead of time. 
	new_dirname = os.path.join(save_path, '%s_submissionid_%d' % (design_popularity[submissionid], submissionid))
	if os.path.exists(new_dirname):
		print '%s already exists, moving on.' % new_dirname
	else:
		try:
			os.makedirs(new_dirname)
		except:
			print 'Unable to create directory %s.' % new_dirname
			pass
	filename = os.path.join(save_path, '%s_submissionid_%d' % (design_popularity[submissionid], submissionid), 'submissionid_info.csv')
	print 'Writing submissionid info to %s.' % filename
	with open(filename, 'wb') as f:
		writer = csv.writer(f)
		writer.writerow(sub_info_header)
		writer.writerow(submissionid_info[submissionid])

# Split the user attributes data to individual files for each submissionid
user_attributes = {}
wb = xlrd.open_workbook('./data/user_attributes.xls')
datasheet = wb.sheets()[0]
# In [10]: datasheet.row(0)
# Out[10]: 
# [text:u'submissionid',
#  text:u'userid',
#  text:u'# comments',
#  text:u'total # words',
#  text:u'gender',
#  text:u'birthdate',
#  text:u'age (years)',
#  text:u'state',
#  text:u'score']
user_attributes_header = xls_row_values(datasheet.row(0))
for row_index in xrange(1, datasheet.nrows):
	row = datasheet.row(row_index)
	submissionid = int(row[0].value)
	user_attributes[submissionid] = user_attributes.setdefault(submissionid, [])
	user_attributes[submissionid].append(row[1:])

for submissionid in submissionid_info:
	filename = os.path.join(save_path, '%s_submissionid_%d' % (design_popularity[submissionid], submissionid), 'user_attributes.csv')
	print 'Writing user attributes to %s.' % filename
	with open(filename, 'wb') as f:
		writer = csv.writer(f)
		writer.writerow(user_attributes_header[1:])
		for row in user_attributes[submissionid]:
			writer.writerow(xls_row_values(row))

# We need this to find the corresponding product image for each submissionid
sub_to_product = {}
with open('./data/submissionid_productid_pairing.csv', 'rb') as f:
	reader = csv.reader(f)
	for row in list(reader)[1:]:
		submissionid = int(row[0])
		productid = int(row[1])
		sub_to_product[submissionid] = productid
for submissionid in submissionid_info:
	if submissionid in sub_to_product:
		productid = sub_to_product[submissionid]
		image_filename = './data/images/productid_%d.jpg' % productid
		print 'Copying product image %s.' % image_filename
		shutil.copy(image_filename, os.path.join(save_path, '%s_submissionid_%d' % (design_popularity[submissionid], submissionid)))

# Copy the prior co-commenting network file
for submissionid in submissionid_info:
	net_filename = './data/prior_cocomment/all/submissionid_%d.csv' % submissionid
	dest_filename = os.path.join(save_path, 
						'%s_submissionid_%d' % (design_popularity[submissionid], submissionid), 
						'prior_cocommenting_network.csv')
	shutil.copyfile(net_filename, dest_filename)

