# Desired data for each product/design:
#
# submissionid
# productid
# sales quantity
# date voting started on design
# date voting ended on design
# number of comments in first 7 days
# number of unique userids from comments in first 7 days
# URL for product
# URL for design
# Number of userids in prior co-commenting network
# Number of edges in prior co-commenting network

# Create list of all popular and unpopular productids, corresponding submissionids
# For each productid, get total sales quantity
#
# ./data/productids_submissionids_high_sales.csv
# ./data/productids_submissionids_low_sales.csv

# For each submissionid, get dates voting started and ended
# For each submissionid, get number of comments in first 7 days
# For each submissionid, get number of unique userids who commented in first 7 days
#
# ./data/submissionid_stats.csv

# For each submissionid, get URL for design
# For each productid, get URL for design
#
# ??

# For each submissionid, get userid of artist/author
#
# ./data/submissionid_creatoruserid.csv

# For each submissionid, get number of userid nodes in co-commenting network
# For each submissionid, get number of edges in co-commenting network
#
# ./data/prior_cocomment/network_stats.csv
