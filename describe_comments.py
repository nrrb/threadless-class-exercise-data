import openpyxl
import csv
import re

def row_values(row):
	# Obviously this will only work with rows of openpyxl cell objects
	# or some other object with the 'value' attribute
	return map(lambda x: x.value, row)

def clean_text(text):
	if text is None:
		return ''
	if not (isinstance(text, str) or isinstance(text, unicode)):
		text = str(text)
	text = re.sub(r'[^A-Za-z0-9 ]', '', text)
	text = re.sub(r'[ ]+', ' ', text)
	return text.strip()

def word_count(text):
	text = clean_text(text)
	return len(text.split(' '))

user_stats_header = ['submissionid', 'userid', '# comments', 'total # words']
submission_stats_header = ['submissionid', 'date voting started', 'date voting ended', '# comments', '# userids']
with open('./data/stats/submissionid_stats.csv', 'wb') as f_sub_stats:
	stats_writer = csv.DictWriter(f_sub_stats, submission_stats_header)
	stats_writer.writeheader()	
	for design_type in ('popular', 'unpopular'):
		wb = openpyxl.load_workbook('designed_products_%s_comments.xlsx' % design_type)
		# The first row are the column headers, and every subsequent row
		# is data related to a single comment for a particular submissionid
		keys = row_values(wb.worksheets[0].rows[0])
		comments = [dict(zip(keys, row_values(row))) for row in wb.worksheets[0].rows[1:]]
		# Now we create a new data structure that is first indexed by the 
		# submissionid, then by the userid associated with a comment on that
		# submissionid, and then by commentid for that comment, with the value
		# being the number of words in that comment text. 
		# {12345: {5623: {53: 4}}}
		comments_dict = {}
		dates = {}
		for comment in comments:
			userid = comment['comment_userid']
			submissionid = comment['submissionid']
			text = comment['comment_text']
			cid = comment['commentid']
			comments_dict[submissionid] = comments_dict.setdefault(submissionid, {})
			comments_dict[submissionid][userid] = comments_dict[submissionid].setdefault(userid, {})
			comments_dict[submissionid][userid][cid] = word_count(text)
			if submissionid not in dates:
				dates[submissionid] = {'start': comment['date_voting_start'], 'end': comment['date_voting_end']}
		for submissionid in comments_dict:
			date_start = dates[submissionid]['start']
			date_end = dates[submissionid]['end']
			num_userids = len(comments_dict[submissionid].keys())
			num_comments = sum([len(comments_dict[submissionid][userid]) for userid in comments_dict[submissionid]])
			stats_writer.writerow(dict(zip(submission_stats_header, [submissionid, date_start, date_end, num_comments, num_userids])))
			with open('./data/stats/submissionid_%s_userid_stats.csv' % submissionid, 'wb') as f_user_stats:
				dw = csv.DictWriter(f_user_stats, user_stats_header)
				dw.writeheader()
				for userid in comments_dict[submissionid]:
					num_user_comments = len(comments_dict[submissionid][userid])
					num_user_words = sum([comments_dict[submissionid][userid][cid] for cid in comments_dict[submissionid][userid]])
					dw.writerow(dict(zip(user_stats_header, [submissionid, userid, num_user_comments, num_user_words])))
