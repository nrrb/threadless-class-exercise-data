# Create table containing all products from threadless.product that 
# originated from a submitted design (submissionid > 0)

drop table _designed_products;
create table _designed_products as 
	select * 
	from threadless.product 
	where submissionid > 0;

# Create table containing aggregate sales figures for each of the previous
# products. Aggregate sales in two ways, by total number of t-shirts sold
# and total number of orders containing the t-shirt. 

drop table _designed_products_sales;
create table _designed_products_sales as
	select _designed_products.productid as product_id,
	    _designed_products.submissionid as submission_id,
	    sale_item_detail.sale_id as sale_id,
	    sale_item_detail.sale_detail_id as sale_detail_id,
	    sale_item_detail.quantity as quantity,
	    sale_item_detail.price_each as price_each
	from _designed_products inner join sale_item_detail on
	_designed_products.productid = sale_item_detail.product_id;

# Create distribution of products by sales volume to show Brooke, aid 
# in picking popular/unpopular designs

drop table _designed_products_total_sales;
create table _designed_products_total_sales as
	select product_id, 
	    submission_id, 
	    count(submission_id) as total_sales, 
	    sum(quantity) as total_items_sold
	from threadless._designed_products_sales 
	group by submission_id
	order by total_sales;

# Brooke (4/23/2012):
# How about we start with total sales of 25,000 or more for the "high sales" 
# category, and 1000 sales or fewer for the "low sales" category? We only need 
# 10 from each category, so that should be enough to get 10 of reasonable (not 
# too big, not too small) size. Not sure what the degree distribution is on 
# each network, but I'd say networks smaller than 10 or larger than 1000 are 
# probably not helpful. 

drop table _designed_products_high_sales;
create table _designed_products_high_sales as
	select product_id,
			submission_id,
			total_sales
	from threadless._designed_products_total_sales
	where total_sales >= 25000;

drop table _designed_products_low_sales;
create table _designed_products_low_sales as
	select product_id,
			submission_id,
			total_sales
	from threadless._designed_products_total_sales
	where total_sales <= 1000;

# Comments on designs that lead to products, from the first 7
# days that a design is open for voting

drop table _designed_products_comments;
create table _designed_products_comments as
	select _designed_products.productid, 
	    _designed_products.submissionid,
	    submissionInfo.dateaccepted as date_voting_start,
	    adddate(submissionInfo.dateaccepted, interval 7 day) as date_voting_end,
	    blog.blogid,
	    blog_comments.commentid,
	    blog_comments.dateadded as comment_dateadded,
	    blog_comments.userid as comment_userid,
	    blog_comments.post as comment_text,
	    blog_comments.status as comment_status
	from _designed_products, submissionInfo, blog, blog_comments where
	    _designed_products.submissionid = submissionInfo.submissionInfoid and
	    blog.submissionid = _designed_products.submissionid and
	    blog.blogid = blog_comments.blogid and
	    blog_comments.dateadded between submissionInfo.dateaccepted and adddate(submissionInfo.dateaccepted, interval 7 day) and
	    submissionInfo.numcomments > 0
	order by submissionid, comment_dateadded;
    
# Comments on just popular/unpopular products (<1k or >25k sales)

drop table _designed_products_popular_comments;
create table _designed_products_popular_comments as
	select
		_designed_products_comments.*
	from _designed_products_comments, _designed_products_high_sales
	where _designed_products_comments.submissionid = _designed_products_high_sales.submission_id
	order by submissionid, comment_dateadded;

drop table _designed_products_unpopular_comments;
create table _designed_products_unpopular_comments as
	select
		_designed_products_comments.*
	from _designed_products_comments, _designed_products_low_sales
	where _designed_products_comments.submissionid = _designed_products_low_sales.submission_id
	order by submissionid, comment_dateadded;

# Based on users who commented on each submissionid, find all
# past commenting behavior by these users.

drop table _prior_commenting_popular;
create table _prior_commenting_popular as 
	select 
		# Each record in this table represents a pairing of a blogid with a
		# userid, and the userid is related in some way to the submissionid. 
		# Each userid is a user that commented on submissionid's blog in 
		# the first 7 days of voting. The blogid here is any blog that the
		# given userid commented on, prior to the submissionid's first day
		# of voting. The blogid is restricted to those for products and 
		# submissions. 
		# Data for a particular submissionid can be extracted:
		# select 
		# 	prior_blogid, 
		# 	userid 
		# from _prior_commenting_popular 
		# where submissionid = 123456789
		dppc.submissionid,
		blog_comments.blogid as prior_blogid,
		blog_comments.userid
	from threadless.blog_comments, 
		# We need to check with the blog table to see what kind of blog it is.
		threadless.blog,
		# dppc for designed products popular comments
		# We need to constrain the results for each 
		(select distinct 
			_designed_products_popular_comments.submissionid,	
			_designed_products_popular_comments.date_voting_start,
			_designed_products_popular_comments.comment_userid
		from threadless._designed_products_popular_comments) as dppc
	where
		blog_comments.blogid = blog.blogid and
		# We're only interested in prior commenting on blogs for products and 
		# design submissions, so both of these are null then it's some other 
		# kind of blog we don't want to see. 
		(blog.submissionid is not null or blog.productid is not null) and
		# Other statuses are "hidden", "deleted", "show" - the vast majority
		# seem to be "visible". We clearly don't want to include invisible
		# comments. 
		blog_comments.status = "visible" and
		# We only want to look at comments made before voting started on the
		# given submissionid. This may include comments a user made on another
		# submissionid after the 7 days had passed for that submission, but
		# it's unavoidable if we're also including prior comments on product
		# blogs. 
		blog_comments.dateadded < dppc.date_voting_start and
		# We only want comments from the original set of users who commented
		# on a given submissionid's blog
		blog_comments.userid = dppc.comment_userid;

drop table _prior_commenting_unpopular;
create table _prior_commenting_unpopular as 
	select 
		# Each record in this table represents a pairing of a blogid with a
		# userid, and the userid is related in some way to the submissionid. 
		# Each userid is a user that commented on submissionid's blog in 
		# the first 7 days of voting. The blogid here is any blog that the
		# given userid commented on, prior to the submissionid's first day
		# of voting. The blogid is restricted to those for products and 
		# submissions. 
		# Data for a particular submissionid can be extracted:
		# select 
		# 	prior_blogid, 
		# 	userid 
		# from _prior_commenting_unpopular 
		# where submissionid = 123456789
		dppc.submissionid,
		blog_comments.blogid as prior_blogid,
		blog_comments.userid
	from threadless.blog_comments, 
		# We need to check with the blog table to see what kind of blog it is.
		threadless.blog,
		# dppc for designed products unpopular comments
		# We need to constrain the results for each 
		(select distinct 
			_designed_products_unpopular_comments.submissionid,	
			_designed_products_unpopular_comments.date_voting_start,
			_designed_products_unpopular_comments.comment_userid
		from threadless._designed_products_unpopular_comments) as dppc
	where
		blog_comments.blogid = blog.blogid and
		# We're only interested in prior commenting on blogs for products and 
		# design submissions, so both of these are null then it's some other 
		# kind of blog we don't want to see. 
		(blog.submissionid is not null or blog.productid is not null) and
		# Other statuses are "hidden", "deleted", "show" - the vast majority
		# seem to be "visible". We clearly don't want to include invisible
		# comments. 
		blog_comments.status = "visible" and
		# We only want to look at comments made before voting started on the
		# given submissionid. This may include comments a user made on another
		# submissionid after the 7 days had passed for that submission, but
		# it's unavoidable if we're also including prior comments on product
		# blogs. 
		blog_comments.dateadded < dppc.date_voting_start and
		# We only want comments from the original set of users who commented
		# on a given submissionid's blog
		blog_comments.userid = dppc.comment_userid;

# Get userid of designer of each submission, as available
select distinct
    submissionInfo.submissionInfoid as submissionid, 
    submissionInfo.userid 
from threadless.submissionInfo, threadless._designed_products_high_sales, threadless._designed_products_low_sales
where submissionInfo.userid > 1 and
    (submissionInfo.submissionInfoid = _designed_products_high_sales.submission_id or
        submissionInfo.submissionInfoid = _designed_products_low_sales.submission_id)
order by submissionInfoid;

# Get some attribute information on each user who left a comment on any
# of the designs
drop table if exists threadless._users;
create table threadless._users as
    select distinct allusers.comment_userid as userid
    from
        ((select distinct 
            _designed_products_popular_comments.comment_userid 
        from 
            threadless._designed_products_popular_comments)
        UNION
        (SELECT distinct
            _designed_products_unpopular_comments.comment_userid
        FROM
            threadless._designed_products_unpopular_comments)) as allusers;

drop table if exists _user_attributes;
create table _user_attributes as
SELECT distinct
    users.userid, 
    users.gender, 
    users.birthdate, 
    users.state 
FROM 
    threadless.users, 
    threadless._users
where 
    users.userid = _users.userid;

# Get ratings that the above users made on the respective designs
drop table if exists _submissionid_userid_pairs;
create table _submissionid_userid_pairs as
select temp.* 
from
    ((SELECT distinct 
        submissionid, 
        comment_userid as userid
    FROM 
        threadless._designed_products_unpopular_comments)
    union
    (select distinct
        submissionid,
        comment_userid as userid
    from
        threadless._designed_products_popular_comments)) as temp;

drop table if exists _submissionid_userid_scores;
create table _submissionid_userid_scores as
select 
	_submissionid_userid_pairs.submissionid as submissionid,
	_submissionid_userid_pairs.userid as userid,
	submissionScoring.score as score
from
	threadless._submissionid_userid_pairs inner join 
	threadless.submissionScoring
on
	_submissionid_userid_pairs.submissionid = submissionScoring.submissionid and
	_submissionid_userid_pairs.userid = submissionScoring.userid;